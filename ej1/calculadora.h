#include <iostream>
using namespace std;

#ifndef CALCULADORA_H
#define CALCULADORA_H

class Calculadora{

    private:
        int largo;
        int suma = 0;
        int *arreglo = NULL;
    public:
        Calculadora(int num, int *arreglo);
        void imprime_arreglo();
        void calculo_suma_cuadrados();
};
#endif
