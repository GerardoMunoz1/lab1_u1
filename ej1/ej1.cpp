#include <iostream>
#include "calculadora.h"

using namespace std;

int main() {

    int *num_list = NULL; // arreglo vacio
    int num; // limite del arreglo

    cout << "¿Cuántos datos desea ingresar?: ";
    cin >> num;

    num_list = new int[num]; // se establece el valor limiite del arreglo

    // ciclo que recorre y guarda en el arreglo
    for (int i=0; i<num; i++){
        cout << "Digite un número: ";
        cin >> num_list[i];
    }

    Calculadora calculadora = Calculadora(num, num_list);
    calculadora.imprime_arreglo();
    calculadora.calculo_suma_cuadrados();
}
